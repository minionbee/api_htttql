package minionbee.htttql.api.rest_controller;

import minionbee.htttql.api.converter.*;
import minionbee.htttql.api.dto.*;
import minionbee.htttql.api.entity.*;
import minionbee.htttql.api.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserRestController {
    @Autowired
    private UserService userService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private BoardService boardService;
    @Autowired
    private DutyService dutyService;
    @Autowired
    private ListBoardService listBoardService;
    @Autowired
    private CardService cardService;
    @Autowired
    private CheckListService checkListService;
    @Autowired
    private DebateService debateService;
    @Autowired
    private AttendeeSerivce attendeeSerivce;
//  department
    @GetMapping("/department")
    public ResponseEntity<?> getAllDepartments(){
        return ResponseEntity.ok(departmentService.getAllDepartment());
    }
//    user
    // get all member in department lead by leader x
    @GetMapping("/department/member/{leader_id}")
    public ResponseEntity<?> getAllUserByDepartment(@PathVariable String leader_id){
        User user=userService.findUserById(Integer.parseInt(leader_id));
        Department department=departmentService.getDepartmentById(user.getDepartment().getId());
        List<User> members_in_team_of_leader=userService.getAllByDepartment(department);
        List<UserDTO> userDTOS=new ArrayList<>();
        members_in_team_of_leader.remove(user);
        for(User user_in_department:members_in_team_of_leader){
            UserDTO userDTO= UserConverter.convertToUserDto(user_in_department);
            userDTOS.add(userDTO);
        }
        return ResponseEntity.ok(userDTOS);
    }
//  user login
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody UserDTO userDTO){
        User user=userService.findByUsernameAndPassword(userDTO.getUsername(),userDTO.getPassword());
        if(user==null) return ResponseEntity.ok().body("Error username or password");
        return ResponseEntity.ok(user);
    }
    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody UserDTO userDTO){
    //        System.out.println(user.getDepartment().getName());
        Department department=departmentService.getDepartmentById(Integer.parseInt(userDTO.getDepartment_name()));
        User new_user=new User();
        new_user.setDepartment(department);
        new_user.setAddress(userDTO.getAddress());
        new_user.setEmail(userDTO.getEmail());
        new_user.setFull_name(userDTO.getFull_name());
        new_user.setPassword(userDTO.getPassword());
        new_user.setUsername(userDTO.getUsername());
        new_user.setPhone_number(userDTO.getPhone_number());
        new_user.setRole(userDTO.getRole());
        userService.save(new_user);
        return ResponseEntity.ok(userDTO);
    }
//    board
    @GetMapping("/board/{id}/{username}")
    public  ResponseEntity<?> getBoardById(@PathVariable int id,@PathVariable String username){
        Board board=boardService.getBoardById(id);
        BoardDTO boardDTO = BoardConverter.convertBoardToBoardDTO(board);
        List<ListBoard> listBoards = listBoardService.getAllByBoard(board);
        List<ListDTO> listDTOS = new ArrayList<>();
        for(ListBoard listBoard:listBoards){
            ListDTO listDTO=ListBoardConverter.convertListToListDTO(listBoard);
            List<Card> cards= cardService.getAllByList(listBoard);
            List<CardDTO> cardDTOS = new ArrayList<>();
            for(Card card:cards)
            {
                CardDTO cardDTO = CardConverter.convertToCardDTO(card);
                List<Attendee> attendees=new ArrayList<>();
                attendees=attendeeSerivce.getAllAttendeeInACard(card);
                cardDTO.setAttendees(attendees);
                cardDTOS.add(cardDTO);
            }
            listDTO.setCardDTOS(cardDTOS);
            listDTOS.add(listDTO);
        }
        boardDTO.setLists(listDTOS);
        return ResponseEntity.ok(boardDTO);
    }
    @PostMapping("/board/{username}")
    public ResponseEntity<?> addNewBoard(@RequestBody Board board,@PathVariable String username){
        User user=userService.findUserByUsername(username);
        boardService.save(board);
        Duty duty=new Duty();
        duty.setBoard(board);
        duty.setUser(user);
        duty.setRole(user.getRole());
        dutyService.save(duty);
        return ResponseEntity.ok(board);
    }
    @PutMapping("/board")
    public ResponseEntity<?> updateABoard(@RequestBody Board board){
        Board board_update=boardService.getBoardById(board.getId());
        board_update.setName(board.getName());
        boardService.save(board_update);
        return ResponseEntity.ok(board_update);
    }
    @DeleteMapping("/board/{board_id}")
    public ResponseEntity<?> deleteABoardById(@PathVariable int board_id){
        Board board_deleted=boardService.getBoardById(board_id);
        List<Duty> duty_deleted= dutyService.getAllByBoard(board_deleted);
        for(Duty duty:duty_deleted)
            dutyService.delete(duty);
        List<ListBoard> lists = listBoardService.getAllByBoard(board_deleted);
        for(ListBoard listBoard : lists) {
            List<Card> cards= cardService.getAllByList(listBoard);
            for(Card card:cards){
                // delete all checklist
                List<CheckList> checkLists = checkListService.getAllDetailByCard(card);
                for(CheckList checkList : checkLists)
                    checkListService.delete(checkList);
                // delete all debate
                List<Debate> debates = debateService.getAllByCard(card);
                for (Debate debate:debates)
                    debateService.delete(debate);
                // delete attendee
                List<Attendee> attendees = attendeeSerivce.getAllAttendeeInACard(card);
                for(Attendee attendee:attendees)
                    attendeeSerivce.delete(attendee);
                // delete card
                cardService.delete(card);
            }
            // delete list board
            listBoardService.delete(listBoard);
        }
        // delete board
        boardService.delete(board_deleted);
        return ResponseEntity.ok(board_deleted);
    }
    // get all board by leader
    //    @GetMapping("/board/{leader_username}")
    //    public ResponseEntity<?> getAllBoardByLeader(@PathVariable String leader_username){
    //        return ResponseEntity.ok(boardService.)
    //    }
//    coordinator
    @GetMapping("/duty/user/{user_id}")
    public ResponseEntity<?> getAllDutiesByUser(@PathVariable int user_id){
        User user=userService.findUserById(user_id);
        List<Duty> duties=dutyService.getAllByUser(user);
        List<DutyDTO> dutyDTOS=new ArrayList<>();
        for(Duty duty:duties)
            dutyDTOS.add(DutyConverter.convertDutyToDutyDTO(duty));
        return ResponseEntity.ok(dutyDTOS);
    }
//    Tag
//    @PostMapping("/tag/{board_id}/{list_id}/{user_id}")
//    public ResponseEntity<?> addNewTag(@RequestBody Card card, @PathVariable int board_id,
//                                       @PathVariable int list_id,@PathVariable String user_id){
    @PostMapping("/tag/{list_id}/{user_id}")
    public ResponseEntity<?> addNewTag(@RequestBody Card card,
                                       @PathVariable int list_id,@PathVariable String user_id){
//        Board board=boardService.getBoardById(board_id);
        ListBoard listBoard=listBoardService.getById(list_id);
        Card card_new=new Card();
        card_new.setIsort(cardService.getAllByList(listBoard).size()+1);
        card_new.setLabel(card.getLabel());
        card_new.setName(card.getName());
        card_new.setStart_date(card.getStart_date());
        card_new.setEnd_date(card.getEnd_date());
        card_new.setListBoard(listBoard);
        cardService.save(card_new);
    //        name of member doing this tag
        if(user_id!=null){
            User user=userService.findUserById(Integer.parseInt(user_id));
            Attendee attendee=new Attendee();
            attendee.setCard(card_new);
            attendee.setUser(user);
            attendeeSerivce.save(attendee);
        }
        return ResponseEntity.ok(card_new);
    }
    //    update user trong 1 card
    @PutMapping("/tag/{card_id}/{is_add}/{user_id}")
    public ResponseEntity<?> updateTag(@PathVariable int card_id,
                                       @PathVariable String user_id, @PathVariable int is_add){
        Card card_update= cardService.getCardById(card_id);
//        add new user in card
        if(is_add==1){
            User user=userService.findUserById(Integer.parseInt(user_id));
            Attendee new_attendee = new Attendee();
            new_attendee.setCard(card_update);
            new_attendee.setUser(user);
            attendeeSerivce.save(new_attendee);
        }
//        delete a user in card
        else if(is_add==0 && user_id!=null){
            List<Attendee> attendees=attendeeSerivce.getAllAttendeeInACard(card_update);
            for(Attendee attendee:attendees){
                if(attendee.getUser().getId()==Integer.parseInt(user_id))
                {
                    attendeeSerivce.delete(attendee);
                    break;
                }
            }
        }
//        delete all user in card
        else if(is_add==0 && user_id==null){
            List<Attendee> attendees=attendeeSerivce.getAllAttendeeInACard(card_update);
            for(Attendee attendee:attendees){
                attendeeSerivce.delete(attendee);
            }
        }
        CardDTO cardDTO=CardConverter.convertToCardDTO(card_update);
        List<Attendee> attendees=attendeeSerivce.getAllAttendeeInACard(card_update);
        cardDTO.setAttendees(attendees);
        return ResponseEntity.ok(cardDTO);
    }
    // get all tag by board
    @GetMapping("/tag/{board_id}")
    public ResponseEntity<?> getAllTagByBoard(@PathVariable int board_id){
        Board board=boardService.getBoardById(board_id);
        List<Card> cardList=cardService.getAllByList(listBoardService.getByBoardAndType(board,"todo"));
        return ResponseEntity.ok(cardList);
    }
    // delete a tag
    @DeleteMapping("/tag/{tag_id}")
    public void deleteATagById(@PathVariable int tag_id){
        Card card_deleted= cardService.getCardById(tag_id);
        // if this tag is the last tag of this user in this board => delete coordinator contain this board and this user
        ListBoard list_Board_contain_tag_delete = listBoardService.getById(card_deleted.getListBoard().getId());
        Board board_contain_list=boardService.getBoardById(list_Board_contain_tag_delete.getBoard().getId());
        List<ListBoard> lists= listBoardService.getAllByBoard(board_contain_list);

        // delete list detail of this tag
        List<CheckList> checkLists = checkListService.getAllDetailByCard(card_deleted);
        for(CheckList checkList : checkLists)
            checkListService.delete(checkList);
        // delete all debates in this tag
        List<Debate> debates = debateService.getAllByCard(card_deleted);
        for (Debate debate:debates)
            debateService.delete(debate);
        // delete all attendees
        List<Attendee> attendees = attendeeSerivce.getAllAttendeeInACard(card_deleted);
        for (Attendee attendee:attendees)
            attendeeSerivce.delete(attendee);
        // update index in list
        ListBoard list_contain_card = listBoardService.getById(card_deleted.getListBoard().getId());
        List<Card> cards_update = cardService.getAllByList(list_contain_card);
        if(card_deleted.getIsort()==1){
            for(Card card_update:cards_update){
                card_update.setIsort(card_update.getIsort()-1);
                cardService.save(card_update);
            }
        }
        if(card_deleted.getIsort()<cards_update.size() && card_deleted.getIsort()!=1){
            for(Card card_update:cards_update){
                if(card_update.getIsort()>card_deleted.getIsort()) {
                    card_update.setIsort(card_update.getIsort() - 1);
                    cardService.save(card_update);
                }
            }
        }
        // delete tag
        cardService.delete(card_deleted);
    // return ResponseEntity.ok(tag_deleted);
    }
//  detail
    @GetMapping("/detail/{tag_id}")
    public ResponseEntity<?> getAllDetailByTag(@PathVariable int tag_id){
        Card card= cardService.getCardById(tag_id);
        return ResponseEntity.ok(checkListService.getAllDetailByCard(card));
    }
    @PostMapping("/detail/{tag_id}")
    public ResponseEntity<?> addNewDetail(@RequestBody CheckList checkListRequest, @PathVariable int tag_id){
        CheckList checkList =new CheckList();
        Card card= cardService.getCardById(tag_id);
        checkList.setName(checkListRequest.getName());
        checkList.setStatus("doing");
        checkList.setCard(card);
        checkList.setChecklist_time(checkListRequest.getChecklist_time());
        //System.out.println(tag.getName_tag()+" "+tag.getId());
        checkListService.save(checkList);
        return ResponseEntity.ok(CheckListConverter.convertToDetailDTO(checkList));
    }
    @PutMapping("/detail/{detail_id}")
    public ResponseEntity<?> updateDetail(@RequestBody CheckList checkListRequest, @PathVariable int detail_id){
        CheckList checkList = checkListService.getById(detail_id);
        if(checkListRequest.getName()!="" && checkListRequest.getName()!=null)
            checkList.setName(checkListRequest.getName());
        checkList.setStatus(checkListRequest.getStatus());
        checkList.setChecklist_time(checkListRequest.getChecklist_time());
        checkListService.save(checkList);
        return ResponseEntity.ok(CheckListConverter.convertToDetailDTO(checkList));
    }
    @DeleteMapping("/detail/{detail_id}")
    public ResponseEntity<?> deleteDetail(@PathVariable int detail_id){
        CheckList checkList_deleted = checkListService.getById(detail_id);
        checkListService.delete(checkList_deleted);
        return ResponseEntity.ok(CheckListConverter.convertToDetailDTO(checkList_deleted));
    }

//    search board by keyword
    @GetMapping("/attendee/user/{card_id}")
    public ResponseEntity<?> getAllUsersByInACard(@PathVariable int card_id){
        Card card_contain_users = cardService.getCardById(card_id);
        List<Attendee> attendees = attendeeSerivce.getAllAttendeeInACard(card_contain_users);
        List<User> users = new ArrayList<>();
        for (Attendee attendee:attendees)
            users.add(attendee.getUser());
        return ResponseEntity.ok(users);
    }
//
    @GetMapping("/users/a/{board_id}")
    public ResponseEntity<?> getAllUserAvailableForBoard(@PathVariable int board_id){
        Board board = boardService.getBoardById(board_id);
        List<Duty> duties = dutyService.getAllByBoard(board);
        List<User> users = userService.getAllUser();
        List<UserDTO> userDTOS=new ArrayList<>();
        for(User user:users){
            UserDTO userDTO=UserConverter.convertToUserDto(user);
            userDTO.setIs_user_in_board(0);
            for(Duty duty:duties){
                if(duty.getUser().getId()==userDTO.getId()){
                    userDTO.setIs_user_in_board(1);
                    break;
                }
            }
            if(!userDTO.getRole().equalsIgnoreCase("Division leader") &&
                    !userDTO.getRole().equalsIgnoreCase("Project manager") &&
                    !userDTO.getRole().equalsIgnoreCase("Administrator")){
                userDTOS.add(userDTO);
            }
        }
        return ResponseEntity.ok(userDTOS);
    }
}
