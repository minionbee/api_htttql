package minionbee.htttql.api.rest_controller;

import minionbee.htttql.api.converter.DutyConverter;
import minionbee.htttql.api.dto.DutyDTO;
import minionbee.htttql.api.entity.Board;
import minionbee.htttql.api.entity.Duty;
import minionbee.htttql.api.entity.User;
import minionbee.htttql.api.service.BoardService;
import minionbee.htttql.api.service.DutyService;
import minionbee.htttql.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/duty")
public class DutyRestController {
    @Autowired
    private DutyService dutyService;
    @Autowired
    private BoardService boardService;
    @Autowired
    private UserService userService;
//    get all users are working currently in a board
    @GetMapping("/board/{board_id}")
    public ResponseEntity<?> getAllUserInABoard(@PathVariable int board_id){
        List<Duty> duties = dutyService.getAllByBoard(boardService.getBoardById(board_id));
        List<DutyDTO> dutyDTOS = new ArrayList<>();
        for(Duty duty:duties) {
            if(!duty.getUser().getRole().equalsIgnoreCase("Project manager") &&
                    !duty.getUser().getRole().equalsIgnoreCase("Division leader"))
                dutyDTOS.add(DutyConverter.convertDutyToDutyDTO(duty));
        }
        return ResponseEntity.ok(dutyDTOS);
    }
//  add a new member to board
    @PostMapping("/p/{board_id}/{user_id}")
    public ResponseEntity<?> addANewMemberToBoard(@PathVariable int board_id, @PathVariable int user_id){
        Duty added_duty=new Duty();
        User user=userService.findUserById(user_id);
        Board board=boardService.getBoardById(board_id);
        added_duty.setBoard(board);
        added_duty.setUser(user);
        added_duty.setRole(user.getRole());
        dutyService.save(added_duty);
        return ResponseEntity.ok(added_duty);
    }
//    delete a duty
    @DeleteMapping("/d/{board_id}/{user_id}")
    public ResponseEntity<?> deleteADuty(@PathVariable int board_id, @PathVariable int user_id){
        User user=userService.findUserById(user_id);
        Board board=boardService.getBoardById(board_id);
        Duty deleted_duty=dutyService.getDutyByUserAndBoard(user,board);
        dutyService.delete(deleted_duty);
        return ResponseEntity.ok(deleted_duty);
    }
}
