package minionbee.htttql.api.rest_controller;


import minionbee.htttql.api.entity.*;
import minionbee.htttql.api.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/card")
public class CardRestController {
    @Autowired
    private CardService cardService;
    @Autowired
    private ListBoardService listBoardService;
    @Autowired
    private BoardService boardService;
    @Autowired
    private CheckListService checkListService;
    @Autowired
    private DebateService debateService;
    @Autowired
    private UserService userService;
    @Autowired
    private AttendeeSerivce attendeeSerivce;
//    Update index card in a same list
    @PutMapping("/{card_id}/{list_id}/{pre_index}/{new_index}")
    public ResponseEntity<?> moveACardInASameList(@PathVariable int list_id, @PathVariable int card_id,
                                                   @PathVariable int pre_index, @PathVariable int new_index){
        ListBoard listBoard_contain_card = listBoardService.getById(list_id);
        Card updated_card = cardService.getCardById(card_id);
        List<Card> cards=cardService.getAllByList(listBoard_contain_card);
//        set new index
        if(pre_index>new_index){
            for(Card card:cards){
                if(card.getIsort()>=new_index && card.getIsort()<pre_index){
                    card.setIsort(card.getIsort()+1);
                    cardService.save(card);
                }
            }
        }
        else{
            for(Card card:cards){
                if(card.getIsort()<=new_index && card.getIsort()>pre_index){
                    card.setIsort(card.getIsort()-1);
                    cardService.save(card);
                }
            }
        }

//        save to table Card
        updated_card.setIsort(new_index);
        cardService.save(updated_card);
        return null;
    }
//    Move a card between two lists
    @PutMapping("/{card_id}/{pre_list_id}/{new_list_id}/{pre_index}/{new_index}")
    public ResponseEntity<?> moveACardBetweenTwoList(@PathVariable int card_id, @PathVariable String pre_list_id,
                                                     @PathVariable String new_list_id,
                                                     @PathVariable int pre_index,
                                                     @PathVariable int new_index){
        ListBoard new_list_board = listBoardService.getById(Integer.parseInt(new_list_id));
        List<Card> new_cards = cardService.getAllByList(new_list_board);
//        process recalculate isort in new list cards
        if (new_cards.size()>0){
            if(new_index == 1){
                for(Card card:new_cards){
                    card.setIsort(card.getIsort()+1);
                    cardService.save(card);
                }
            }
            else if(new_index>1){
                for(Card card:new_cards){
                    if(card.getIsort()>=new_index) {
                        card.setIsort(card.getIsort()+1);
                        cardService.save(card);
                    }
                }
            }
        }
//        Move card to new list
        Card card_moved = cardService.getCardById(card_id);
        card_moved.setIsort(new_index);
        card_moved.setListBoard(new_list_board);
        cardService.save(card_moved);
//        process recalculate isort in pre list cards
        ListBoard pre_list_board = listBoardService.getById(Integer.parseInt(pre_list_id));
        List<Card> pre_cards = cardService.getAllByList(pre_list_board);
        if (pre_cards.size()>0){
            if(pre_index == 1){
                for(Card card:pre_cards){
                    card.setIsort(card.getIsort()-1);
                    cardService.save(card);
                }
            }
            else if(pre_index>1){
                for(Card card:pre_cards){
                    if(card.getIsort()>pre_index){
                        card.setIsort(card.getIsort()-1);
                        cardService.save(card);
                    }
                }
            }
        }
        return null;
    }
    @PostMapping("/{card_id}/{user_id}")
    public ResponseEntity<?> addAUserToCard(@PathVariable int user_id,
                                            @PathVariable int card_id){
        Attendee attendee = new Attendee();
        attendee.setUser(userService.findUserById(user_id));
        attendee.setCard(cardService.getCardById(card_id));
        attendeeSerivce.save(attendee);
        return ResponseEntity.ok(null);
    }
}
