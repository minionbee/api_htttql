package minionbee.htttql.api.rest_controller;

import minionbee.htttql.api.entity.*;
import minionbee.htttql.api.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class AdminRestController {
    @Autowired
    private UserService userService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private DutyService dutyService;
    @Autowired
    private CardService cardService;
    @Autowired
    private CheckListService checkListService;
    @Autowired
    private BoardService boardService;
    @Autowired
    private ListBoardService listBoardService;
    @Autowired
    private AttendeeSerivce attendeeSerivce;
//    get all user
    @GetMapping("/user")
    public ResponseEntity<?> getAllUsers(){
        List<User> users=userService.getAllUser();
        return  ResponseEntity.ok(users);
    }
//    get a user
    @GetMapping("/user/{username}")
    public ResponseEntity<?> getUserByUsername(@PathVariable String username){
        return ResponseEntity.ok(userService.findUserByUsername(username));
    }
    //    @GetMapping("/user/{user_id}")
    //    public ResponseEntity<?> getUserById(@PathVariable int user_id){
    //        return ResponseEntity.ok(userService.findUserById(user_id));
    //    }
//    get all user in a department
    @GetMapping("/user/department/{department_id}")
    public ResponseEntity<?> getAllUsersInADepartment(@PathVariable int department_id){
        Department department = departmentService.getDepartmentById(department_id);
        return ResponseEntity.ok(userService.getAllByDepartment(department));
    }
//
    @DeleteMapping("/user/{user_id}")
    public ResponseEntity<?> deleteUserById(@PathVariable int user_id){
        User user_delete=userService.findUserById(user_id);
        List<Attendee> attendees=attendeeSerivce.getAllByUser(user_delete);
        for(int i = 0; i<attendees.size(); i++)
            attendeeSerivce.delete(attendees.get(i));
        userService.delete(user_delete);
        return ResponseEntity.ok(user_delete);
    }
    @PutMapping("/user/{department_id}")
    public ResponseEntity<?> updateUser(@RequestBody User user,@PathVariable int department_id){
        User user_update=userService.findUserByUsername(user.getUsername());
        user_update.setPassword(user.getPassword());
        user_update.setFull_name(user.getFull_name());
        user_update.setRole(user.getRole());
        user_update.setAddress(user.getAddress());
        user_update.setEmail(user.getEmail());
        user_update.setPhone_number(user.getPhone_number());
        user_update.setDepartment(departmentService.getDepartmentById(department_id));
        userService.save(user_update);
        return ResponseEntity.ok(user_update);
    }
//    Department
    @PostMapping("/department")
    public ResponseEntity<?> addDepartment(@RequestBody Department department){
        departmentService.save(department);
        return ResponseEntity.ok(department);
    }
    @GetMapping("/department/{department_id}")
    public ResponseEntity<?> getDepartmentById(@PathVariable int department_id){
        return ResponseEntity.ok(departmentService.getDepartmentById(department_id));
    }
    @PutMapping("/department")
    public ResponseEntity<?> updateADepartment(@RequestBody Department department){
        Department department_update=departmentService.getDepartmentById(department.getId());
        department_update.setName(department.getName());
        department_update.setLocation(department.getLocation());
        departmentService.save(department_update);
        return ResponseEntity.ok(department_update);
    }
    @DeleteMapping("/department/{department_id}")
    public ResponseEntity<?> deleteADepartment(@PathVariable int department_id){
        Department department_deleted=departmentService.getDepartmentById(department_id);
        List<User> users=userService.getAllByDepartment(department_deleted);

        List<Board> list_board_deleted=new ArrayList<>();
        List<User> list_user_deleted=new ArrayList<>();

        for(User user:users){
        //  System.out.println(users.get(i).getUsername());
            list_user_deleted.add(user);
            List<Duty> dutys= dutyService.getAllByUser(user);
            for(Duty duty:dutys){
                if(duty.getRole().equalsIgnoreCase("leader")){
                    Board board_deleted=duty.getBoard();
                    list_board_deleted.add(board_deleted);
                    List<ListBoard> lists = listBoardService.getAllByBoard(board_deleted);
                    for(ListBoard listBoard : lists){
                        List<Card> cards= cardService.getAllByList(listBoard);
                        for(Card card:cards){
                            List<CheckList> checkLists = checkListService.getAllDetailByCard(card);
                            for(CheckList checkList : checkLists){
                                checkListService.delete(checkList);
                            }
                            cardService.delete(card);
                        }
                        listBoardService.delete(listBoard);
                    }
                }
                dutyService.delete(duty);
            }
        }
        for(Board board:list_board_deleted)boardService.delete(board);
        for(User user:list_user_deleted)userService.delete(user);
        departmentService.delete(department_deleted);
        return ResponseEntity.ok(department_deleted);
    }
}
