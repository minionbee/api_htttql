package minionbee.htttql.api.rest_controller;

import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.Debate;
import minionbee.htttql.api.entity.User;
import minionbee.htttql.api.service.CardService;
import minionbee.htttql.api.service.DebateService;
import minionbee.htttql.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CommentRestController {
    @Autowired
    private DebateService debateService;
    @Autowired
    private CardService cardService;
    @Autowired
    private UserService userService;
//    Get all comment
    @GetMapping("/debate/{card_id}")
    public ResponseEntity<?> getAllComment(@PathVariable int card_id){
        Card card_contain_debate=cardService.getCardById(card_id);
        return ResponseEntity.ok(debateService.getAllByCard(card_contain_debate));
    }
//    Post a new comment
    @PostMapping("/debate/{card_id}")
    public ResponseEntity<?> addNewComment(@RequestBody Debate sour_debate,
                                           @PathVariable int card_id){
//    Get card contain comment
        Card card_contain_debate=cardService.getCardById(card_id);
        Debate added_debate=new Debate();
        added_debate.setContent(sour_debate.getContent());
        added_debate.setTime(sour_debate.getTime());
        added_debate.setCard(card_contain_debate);
        added_debate.setEmployee(sour_debate.getEmployee());
        debateService.save(added_debate);
        return ResponseEntity.ok(added_debate);
    }
//    Delete a comment
    @DeleteMapping("/debate/{debate_id}")
    public ResponseEntity<?> deleteAComment(@PathVariable int debate_id){
        Debate deleted_debate=debateService.getByDebateId(debate_id);
        debateService.delete(deleted_debate);
        return ResponseEntity.ok(deleted_debate);
    }
//    Put a comment
    @PutMapping("/debate")
    public ResponseEntity<?> updateAComment(@RequestBody Debate debate){
        Debate updated_comment=debateService.getByDebateId(debate.getId());
        updated_comment.setContent(debate.getContent());
        updated_comment.setTime(debate.getTime());
        debateService.save(updated_comment);
        return ResponseEntity.ok(updated_comment);
    }
}
