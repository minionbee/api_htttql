package minionbee.htttql.api.rest_controller;

import minionbee.htttql.api.entity.Attendee;
import minionbee.htttql.api.service.AttendeeSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/attendee")
public class AttendeeRestController {
    @Autowired
    private AttendeeSerivce attendeeSerivce;
//
    @DeleteMapping("/{attendee_id}")
    public ResponseEntity<?> deleteUserInACard(@PathVariable int attendee_id){
        Attendee attendee = attendeeSerivce.getAttendeeById(attendee_id);
        attendeeSerivce.delete(attendee);
        return ResponseEntity.ok(attendee);
    }
}
