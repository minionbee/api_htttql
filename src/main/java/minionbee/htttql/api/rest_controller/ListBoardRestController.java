package minionbee.htttql.api.rest_controller;

import minionbee.htttql.api.entity.*;
import minionbee.htttql.api.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/list")
public class ListBoardRestController {
    @Autowired
    private ListBoardService listBoardService;
    @Autowired
    private BoardService boardService;
    @Autowired
    private UserService userService;
    @Autowired
    private CardService taskService;
    @Autowired
    private DebateService debateService;
    @Autowired
    private CheckListService checkListService;
    @Autowired
    private AttendeeSerivce attendeeSerivce;
//    Create a list
    @PostMapping("/{board_id}")
    public ResponseEntity<?> addANewList(@RequestBody ListBoard listBoard,
                                         @PathVariable int board_id){
        ListBoard new_list=new ListBoard();
        new_list.setBoard(boardService.getBoardById(board_id));
        new_list.setName(listBoard.getName());
        new_list.setType(listBoard.getType());
        listBoardService.save(new_list);
        return ResponseEntity.ok(new_list);
    }
//    Edit a list
    @PutMapping("/u")
    public ResponseEntity<?> updateAWorkflow(@RequestBody ListBoard workflow){
        ListBoard updated_workflow = listBoardService.getById(workflow.getId());
        updated_workflow.setName(workflow.getName());
        listBoardService.save(updated_workflow);
        return  ResponseEntity.ok(updated_workflow);
    }
    @DeleteMapping("/d/{listboard_id}")
    public ResponseEntity<?> deleteAWorkflow(@PathVariable int listboard_id){
        ListBoard deleted_workflow = listBoardService.getById(listboard_id);
//        delete all tasks in a workflow
        List<Card> tasks = taskService.getAllByList(deleted_workflow);
        for (Card task:tasks){
//            delete all debate of task
            List<Debate> debates = debateService.getAllByCard(task);
            for(Debate debate:debates)
                debateService.delete(debate);
//            delete all checklist of task
            List<CheckList> checkLists = checkListService.getAllDetailByCard(task);
            for(CheckList checkList:checkLists)
                checkListService.delete(checkList);
//            delete all attendee
            List<Attendee> attendees = attendeeSerivce.getAllAttendeeInACard(task);
            for(Attendee attendee:attendees)
                attendeeSerivce.delete(attendee);
//            delete task
            taskService.delete(task);
        }
        listBoardService.delete(deleted_workflow);
        return ResponseEntity.ok(deleted_workflow);
    }
}
