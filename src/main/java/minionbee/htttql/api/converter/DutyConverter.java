package minionbee.htttql.api.converter;

import minionbee.htttql.api.dto.DutyDTO;
import minionbee.htttql.api.entity.Duty;

public class DutyConverter {
    public static DutyDTO convertDutyToDutyDTO(Duty duty){
        DutyDTO dutyDTO = new DutyDTO();
        dutyDTO.setId(duty.getId());
        dutyDTO.setRole(duty.getRole());
        dutyDTO.setUserDTO(UserConverter.convertToUserDto(duty.getUser()));
        dutyDTO.setBoardDTO(BoardConverter.convertBoardToBoardDTO(duty.getBoard()));
        return dutyDTO;
    }
}
