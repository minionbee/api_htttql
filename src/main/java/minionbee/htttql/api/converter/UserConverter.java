package minionbee.htttql.api.converter;


import minionbee.htttql.api.dto.UserDTO;
import minionbee.htttql.api.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {
    public static UserDTO convertToUserDto(User user){
        UserDTO userDTO=new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setUsername(user.getUsername());
        userDTO.setPassword(user.getPassword());
        userDTO.setRole(user.getRole());
        userDTO.setAddress(user.getAddress());
        userDTO.setFull_name(user.getFull_name());
        userDTO.setPhone_number(user.getPhone_number());
        userDTO.setEmail(user.getEmail());
        userDTO.setDepartment_name(user.getDepartment().getName());
        userDTO.setDepartment(user.getDepartment());
        return userDTO;
    }
}
