package minionbee.htttql.api.converter;

import minionbee.htttql.api.dto.CardDTO;
import minionbee.htttql.api.entity.Card;

public class CardConverter {
    public static CardDTO convertToCardDTO(Card card){
        CardDTO cardDTO =new CardDTO();
        cardDTO.setName(card.getName());
        cardDTO.setId(card.getId());
        cardDTO.setStatus(card.getStatus());
        cardDTO.setLabel(card.getLabel());
        cardDTO.setStatus(card.getStatus());
        cardDTO.setListBoard(card.getListBoard());
        cardDTO.setCheckList(card.getCheckList());
        cardDTO.setStart_date(card.getStart_date());
        cardDTO.setEnd_date(card.getEnd_date());
        cardDTO.setIsort(card.getIsort());
        int dem=0;
        for(int i=0;i<card.getCheckList().size();i++)
            if(card.getCheckList().get(i).getStatus().equalsIgnoreCase("complete"))dem+=1;
        if(card.getCheckList().size()!=0){
            cardDTO.setPercent_complete((float)(dem*100)/card.getCheckList().size());
        }
        else cardDTO.setPercent_complete(0);
        return cardDTO;
    }
}
