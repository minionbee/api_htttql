package minionbee.htttql.api.converter;

import minionbee.htttql.api.dto.ListDTO;
import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.ListBoard;
import minionbee.htttql.api.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class ListBoardConverter {
    public static ListDTO convertListToListDTO(ListBoard listBoard){
        ListDTO listDTO=new ListDTO();
        listDTO.setId(listBoard.getId());
        listDTO.setName(listBoard.getName());
        return listDTO;
    }
}
