package minionbee.htttql.api.converter;

import minionbee.htttql.api.dto.BoardDTO;
import minionbee.htttql.api.entity.Board;

public class BoardConverter {
    public static BoardDTO convertBoardToBoardDTO(Board board){
        BoardDTO boardDTO = new BoardDTO();
        boardDTO.setId(board.getId());
        boardDTO.setName(board.getName());
        boardDTO.setBudget(board.getBudget());
        boardDTO.setCurrency(board.getCurrency());
        boardDTO.setDescription(board.getDescription());
        boardDTO.setCreated_date(board.getCreated_date());
        return boardDTO;
    }
}
