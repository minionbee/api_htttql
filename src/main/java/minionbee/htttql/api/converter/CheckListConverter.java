package minionbee.htttql.api.converter;

import minionbee.htttql.api.dto.DetailDTO;
import minionbee.htttql.api.entity.CheckList;

public class CheckListConverter {
    public static DetailDTO convertToDetailDTO(CheckList checkList){
        DetailDTO detailDTO=new DetailDTO();
        detailDTO.setId(checkList.getId());
        detailDTO.setName_detail(checkList.getName());
        detailDTO.setIs_complete(checkList.getStatus());
        detailDTO.setCard(checkList.getCard());
//        detailDTO.setType_list_of_tag(checkList.getCard().getListBoard().getType());
        detailDTO.setChecklist_time(checkList.getChecklist_time());
        return detailDTO;
    }
}
