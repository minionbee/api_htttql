package minionbee.htttql.api.entity;


import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Card")
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    private String name;
    private String label;
    private String status;
    private Date start_date;
    private Date end_date;
    private int isort;
    @ManyToOne
    private ListBoard listBoard;
    @OneToMany(mappedBy = "card", fetch = FetchType.LAZY)
    private List<CheckList> checkList;
    public Card() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public ListBoard getListBoard() {
        return listBoard;
    }

    public void setListBoard(ListBoard listBoard) {
        this.listBoard = listBoard;
    }

    public List<CheckList> getCheckList() {
        return checkList;
    }

    public void setCheckList(List<CheckList> checkList) {
        this.checkList = checkList;
    }

    public int getIsort() {
        return isort;
    }

    public void setIsort(int isort) {
        this.isort = isort;
    }
}
