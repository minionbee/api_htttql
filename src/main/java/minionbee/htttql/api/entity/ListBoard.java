package minionbee.htttql.api.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "list")
public class ListBoard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    private String type;
    private String name;
    @ManyToOne
    private Board board;
//    @OneToMany(mappedBy = "list",fetch = FetchType.LAZY)
//    private List<Card> cards;

    public ListBoard() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    //    public List<Card> getCards() {
//        return cards;
//    }
//
//    public void setCards(List<Card> cards) {
//        this.cards = cards;
//    }
}
