package minionbee.htttql.api.repository;

import minionbee.htttql.api.entity.Department;
import minionbee.htttql.api.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User getByUsername(String username);
    User findByUsernameAndPassword(String username,String password);
    List<User> getAllByDepartment(Department department);
    User findById(int id);
}
