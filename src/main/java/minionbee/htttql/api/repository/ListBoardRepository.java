package minionbee.htttql.api.repository;

import minionbee.htttql.api.entity.Board;
import minionbee.htttql.api.entity.ListBoard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListBoardRepository extends JpaRepository<ListBoard,Long> {
    List<ListBoard> getAllByBoard(Board board);
    ListBoard getByBoardAndType(Board board, String type);
    ListBoard getById(int id);
}
