package minionbee.htttql.api.repository;

import minionbee.htttql.api.entity.Attendee;
import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttendeeRepository extends JpaRepository<Attendee,Long> {
    List<Attendee> getAllAttendeeByCard(Card card);
    List<Attendee> getAllByUser(User user);
    Attendee getByCardAndUser(Card card, User user);
    Attendee getById(int attendee_id);
}
