package minionbee.htttql.api.repository;

import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.Debate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DebateRepository extends JpaRepository<Debate,Long> {
    List<Debate> getAllByCard(Card card);
    Debate getById(int debate_id);
}
