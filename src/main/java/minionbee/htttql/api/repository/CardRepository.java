package minionbee.htttql.api.repository;

import minionbee.htttql.api.entity.CheckList;
import minionbee.htttql.api.entity.ListBoard;
import minionbee.htttql.api.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardRepository extends JpaRepository<Card,Long> {
    List<Card> getAllByListBoard(ListBoard listBoard);
    Card getById(int id);
    Card getByCheckList(CheckList checkList);
    Card getByListBoardAndIsort(ListBoard listBoard,int isort);
}
