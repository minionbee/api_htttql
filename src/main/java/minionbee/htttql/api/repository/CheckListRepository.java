package minionbee.htttql.api.repository;

import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.CheckList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CheckListRepository extends JpaRepository<CheckList,Long> {
    List<CheckList> getAllByCard(Card card);
    CheckList getById(int id);

}
