package minionbee.htttql.api.repository;

import minionbee.htttql.api.entity.Board;
import minionbee.htttql.api.entity.Duty;
import minionbee.htttql.api.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DutyRepository extends JpaRepository<Duty,Long> {
    List<Duty> getAllByUserAndRole(User user,String role);
    Duty getByUserAndBoard(User user, Board board);
    List<Duty> getAllByBoard(Board board);
    List<Duty> getAllByUser(User user);
    Duty getById(int duty_id);
}
