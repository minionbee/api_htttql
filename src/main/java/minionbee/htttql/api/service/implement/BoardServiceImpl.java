package minionbee.htttql.api.service.implement;

import minionbee.htttql.api.entity.Board;
import minionbee.htttql.api.repository.BoardRepository;
import minionbee.htttql.api.service.BoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BoardServiceImpl implements BoardService {
    @Autowired
    private BoardRepository boardRepository;
    @Override
    public Board getBoardById(int id) {
        return boardRepository.getById(id);
    }

    @Override
    public void save(Board board) {
        boardRepository.save(board);
    }

    @Override
    public void delete(Board board) {
        boardRepository.delete(board);
    }
}
