package minionbee.htttql.api.service.implement;

import minionbee.htttql.api.entity.Board;
import minionbee.htttql.api.entity.ListBoard;
import minionbee.htttql.api.repository.ListBoardRepository;
import minionbee.htttql.api.service.ListBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListBoardServiceImpl implements ListBoardService {
    @Autowired
    private ListBoardRepository listBoardRepository;
    @Override
    public List<ListBoard> getAllByBoard(Board board) {
        return listBoardRepository.getAllByBoard(board);
    }

    @Override
    public void save(ListBoard listBoard) {
        listBoardRepository.save(listBoard);
    }

    @Override
    public void delete(ListBoard listBoard) {
        listBoardRepository.delete(listBoard);
    }

    @Override
    public ListBoard getByBoardAndType(Board board, String type) {
        return listBoardRepository.getByBoardAndType(board,type);
    }

    @Override
    public ListBoard getById(int id) {
        return listBoardRepository.getById(id);
    }
}
