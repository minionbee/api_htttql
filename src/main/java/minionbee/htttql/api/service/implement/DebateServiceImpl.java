package minionbee.htttql.api.service.implement;

import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.Debate;
import minionbee.htttql.api.repository.DebateRepository;
import minionbee.htttql.api.service.DebateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DebateServiceImpl implements DebateService {
    @Autowired
    private DebateRepository debateRepository;
    @Override
    public List<Debate> getAllByCard(Card card) {
        return debateRepository.getAllByCard(card);
    }

    @Override
    public Debate getByDebateId(int debate_id) {
        return debateRepository.getById(debate_id);
    }

    @Override
    public void save(Debate debate) {
        debateRepository.save(debate);
    }

    @Override
    public void delete(Debate debate) {
        debateRepository.delete(debate);
    }
}
