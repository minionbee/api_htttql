package minionbee.htttql.api.service.implement;

import minionbee.htttql.api.entity.Department;
import minionbee.htttql.api.repository.DepartmentRepository;
import minionbee.htttql.api.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository;
    @Override
    public List<Department> getAllDepartment() {
        return departmentRepository.findAll();
    }

    @Override
    public void save(Department d) {
        departmentRepository.save(d);
    }

    @Override
    public void delete(Department d) {
        departmentRepository.delete(d);
    }

    @Override
    public Department getDepartmentById(int id) {
        return departmentRepository.getById(id);
    }

    @Override
    public Department getDepartmentByName(String name) {
        return departmentRepository.getByName(name);
    }
}
