package minionbee.htttql.api.service.implement;

import minionbee.htttql.api.entity.Attendee;
import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.User;
import minionbee.htttql.api.repository.AttendeeRepository;
import minionbee.htttql.api.service.AttendeeSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttendeeServiceImpl implements AttendeeSerivce {
    @Autowired
    private AttendeeRepository attendeeRepository;
    @Override
    public void delete(Attendee attendee) {
        attendeeRepository.delete(attendee);
    }

    @Override
    public void save(Attendee attendee) {
        attendeeRepository.save(attendee);
    }

    @Override
    public List<Attendee> getAllAttendeeInACard(Card card) {
        return attendeeRepository.getAllAttendeeByCard(card);
    }

    @Override
    public List<Attendee> getAllByUser(User user) {
        return attendeeRepository.getAllByUser(user);
    }

    @Override
    public Attendee getAttendeeById(int attendee_id) {
        return attendeeRepository.getById(attendee_id);
    }

    @Override
    public Attendee getByCardAndUser(Card card, User user) {
        return attendeeRepository.getByCardAndUser(card,user);
    }
}
