package minionbee.htttql.api.service.implement;

import minionbee.htttql.api.entity.Board;
import minionbee.htttql.api.entity.Duty;
import minionbee.htttql.api.entity.User;
import minionbee.htttql.api.repository.DutyRepository;
import minionbee.htttql.api.service.DutyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DutyServiceImpl implements DutyService {
    @Autowired
    private DutyRepository dutyRepository;

    @Override
    public List<Duty> getAllDutyByUserAndRole(User user, String role) {
        return dutyRepository.getAllByUserAndRole(user,role);
    }

    @Override
    public void save(Duty duty) {
        dutyRepository.save(duty);
    }

    @Override
    public void delete(Duty duty) {
        dutyRepository.delete(duty);
    }

    @Override
    public List<Duty> getAllByBoard(Board board) {
        return dutyRepository.getAllByBoard(board);
    }

    @Override
    public List<Duty> getAllByUser(User user) {
        return dutyRepository.getAllByUser(user);
    }

    @Override
    public Duty getDutyByUserAndBoard(User user, Board board) {
        return dutyRepository.getByUserAndBoard(user,board);
    }
}
