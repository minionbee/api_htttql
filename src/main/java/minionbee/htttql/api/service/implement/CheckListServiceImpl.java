package minionbee.htttql.api.service.implement;

import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.CheckList;
import minionbee.htttql.api.repository.CheckListRepository;
import minionbee.htttql.api.service.CheckListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CheckListServiceImpl implements CheckListService {
    @Autowired
    private CheckListRepository checkListRepository;
    @Override
    public List<CheckList> getAllDetailByCard(Card card) {
        return checkListRepository.getAllByCard(card);
    }

    @Override
    public void save(CheckList checkList) {
        checkListRepository.save(checkList);
    }

    @Override
    public void delete(CheckList checkList) {
        checkListRepository.delete(checkList);
    }

    @Override
    public CheckList getById(int id) {
        return checkListRepository.getById(id);
    }
}
