package minionbee.htttql.api.service.implement;


import minionbee.htttql.api.entity.Department;
import minionbee.htttql.api.entity.User;
import minionbee.htttql.api.repository.UserRepository;
import minionbee.htttql.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public User findByUsernameAndPassword(String username, String password) {
        return userRepository.findByUsernameAndPassword(username,password);
    }

    @Override
    public List<User> getAllByDepartment(Department department) {
        return userRepository.getAllByDepartment(department);
    }

    @Override
    public User findUserById(int id) {
        return userRepository.findById(id);
    }

}
