package minionbee.htttql.api.service.implement;

import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.CheckList;
import minionbee.htttql.api.entity.ListBoard;
import minionbee.htttql.api.entity.User;
import minionbee.htttql.api.repository.CardRepository;
import minionbee.htttql.api.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardServiceImpl implements CardService {
    @Autowired
    private CardRepository cardRepository;

    @Override
    public Card getCardById(int id) {
        return cardRepository.getById(id);
    }

    @Override
    public Card getByCheckList(CheckList checkList) {
        return cardRepository.getByCheckList(checkList);
    }

    @Override
    public void save(Card card) {
        cardRepository.save(card);
    }

    @Override
    public void delete(Card card) {
        cardRepository.delete(card);
    }

    @Override
    public List<Card> getAllByList(ListBoard listBoard) {
        return cardRepository.getAllByListBoard(listBoard);
    }

    @Override
    public Card getByListBoardAndIsort(ListBoard listBoard, int index) {
        return cardRepository.getByListBoardAndIsort(listBoard,index);
    }
}
