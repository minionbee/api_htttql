package minionbee.htttql.api.service;

import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.Debate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DebateService {
    List<Debate> getAllByCard(Card card);
    Debate getByDebateId(int debate_id);
    void save(Debate debate);
    void delete(Debate debate);
}
