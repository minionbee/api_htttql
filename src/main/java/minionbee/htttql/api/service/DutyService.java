package minionbee.htttql.api.service;

import minionbee.htttql.api.entity.Board;
import minionbee.htttql.api.entity.Duty;
import minionbee.htttql.api.entity.User;

import java.util.List;

public interface DutyService {
    List<Duty> getAllDutyByUserAndRole(User user, String role);
    void save(Duty duty);
    void delete(Duty duty);
    List<Duty> getAllByBoard(Board board);
    List<Duty> getAllByUser(User user);
    Duty getDutyByUserAndBoard(User user,Board board);
}
