package minionbee.htttql.api.service;

import minionbee.htttql.api.entity.Attendee;
import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.User;

import java.util.List;

public interface AttendeeSerivce {
    void delete(Attendee attendee);
    void save(Attendee attendee);
    List<Attendee> getAllAttendeeInACard(Card card);
    List<Attendee> getAllByUser(User user);
    Attendee getAttendeeById(int attendee_id);
    Attendee getByCardAndUser(Card card, User user);
}
