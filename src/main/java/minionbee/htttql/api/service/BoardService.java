package minionbee.htttql.api.service;

import minionbee.htttql.api.entity.Board;

public interface BoardService {
    Board getBoardById(int id);
    void save(Board board);
    void delete(Board board);
}
