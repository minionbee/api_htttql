package minionbee.htttql.api.service;

import minionbee.htttql.api.entity.Board;
import minionbee.htttql.api.entity.ListBoard;

import java.util.List;

public interface ListBoardService {
    List<ListBoard> getAllByBoard(Board board);
    void save(ListBoard listBoard);
    void delete(ListBoard listBoard);
    ListBoard getByBoardAndType(Board board, String type);
    ListBoard getById(int id);
}
