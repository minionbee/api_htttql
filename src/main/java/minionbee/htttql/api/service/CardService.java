package minionbee.htttql.api.service;

import minionbee.htttql.api.entity.CheckList;
import minionbee.htttql.api.entity.ListBoard;
import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.User;

import java.util.List;

public interface CardService {
    Card getCardById(int id);
    Card getByCheckList(CheckList checkList);
    void save(Card card);
    void delete(Card card);
    List<Card> getAllByList(ListBoard listBoard);
    Card getByListBoardAndIsort(ListBoard listBoard,int isort);
}
