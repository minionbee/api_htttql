package minionbee.htttql.api.service;

import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.CheckList;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CheckListService {
    List<CheckList> getAllDetailByCard(Card card);
    void save(CheckList checkList);
    void delete(CheckList checkList);
    CheckList getById(int id);
}
