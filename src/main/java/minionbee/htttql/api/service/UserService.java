package minionbee.htttql.api.service;



import minionbee.htttql.api.entity.Department;
import minionbee.htttql.api.entity.User;

import java.util.List;

public interface UserService {
    List<User> getAllUser();
    User findUserByUsername(String username);
    void save(User user);
    void delete(User user);
    User findByUsernameAndPassword(String username,String password);
    List<User> getAllByDepartment(Department department);
    User findUserById(int id);
}
