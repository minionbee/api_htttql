package minionbee.htttql.api.service;

import minionbee.htttql.api.entity.Department;

import java.util.List;

public interface DepartmentService {
    List<Department> getAllDepartment();
    void save(Department d);
    void delete(Department d);
    Department getDepartmentById(int id);
    Department getDepartmentByName(String name);
}
