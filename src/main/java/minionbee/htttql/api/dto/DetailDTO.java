package minionbee.htttql.api.dto;


import minionbee.htttql.api.entity.Card;

import java.sql.Timestamp;

public class DetailDTO {
    private int id;
    private String name_detail;
    private String is_complete;
    private Card card;
    private Timestamp checklist_time;

    public DetailDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName_detail() {
        return name_detail;
    }

    public void setName_detail(String name_detail) {
        this.name_detail = name_detail;
    }

    public String getIs_complete() {
        return is_complete;
    }

    public void setIs_complete(String is_complete) {
        this.is_complete = is_complete;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Timestamp getChecklist_time() {
        return checklist_time;
    }

    public void setChecklist_time(Timestamp checklist_time) {
        this.checklist_time = checklist_time;
    }
}
