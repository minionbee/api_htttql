package minionbee.htttql.api.dto;

import minionbee.htttql.api.entity.Card;
import minionbee.htttql.api.entity.ListBoard;

import java.util.Date;
import java.util.List;

public class BoardDTO {
    private int id;
    private String name;
    private Date created_date;
    private float budget;
    private String description;
    private String currency;
    private List<ListDTO> lists;
    public BoardDTO() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public List<ListDTO> getLists() {
        return lists;
    }

    public void setLists(List<ListDTO> lists) {
        this.lists = lists;
    }

    public float getBudget() {
        return budget;
    }

    public void setBudget(float budget) {
        this.budget = budget;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
