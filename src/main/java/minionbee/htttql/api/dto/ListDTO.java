package minionbee.htttql.api.dto;

import minionbee.htttql.api.entity.Card;

import java.util.List;

public class ListDTO {
    private int id;
    private String name;
    private List<CardDTO> cardDTOS;

    public ListDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CardDTO> getCardDTOS() {
        return cardDTOS;
    }

    public void setCardDTOS(List<CardDTO> cardDTOS) {
        this.cardDTOS = cardDTOS;
    }
}
