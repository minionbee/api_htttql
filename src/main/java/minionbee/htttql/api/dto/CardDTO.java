package minionbee.htttql.api.dto;

import minionbee.htttql.api.entity.Attendee;
import minionbee.htttql.api.entity.CheckList;
import minionbee.htttql.api.entity.ListBoard;
import minionbee.htttql.api.entity.User;

import java.util.Date;
import java.util.List;

public class CardDTO {
    private int id;
    private String name;
    private String label;
    private String status;
    private Date start_date;
    private Date end_date;
    private String user_id;
    private ListBoard listBoard;
    private List<CheckList> checkList;
    private List<Attendee> attendees;
    private float percent_complete;
    private int isort;
    public CardDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public ListBoard getListBoard() {
        return listBoard;
    }

    public void setListBoard(ListBoard listBoard) {
        this.listBoard = listBoard;
    }

    public List<CheckList> getCheckList() {
        return checkList;
    }

    public void setCheckList(List<CheckList> checkList) {
        this.checkList = checkList;
    }

    public List<Attendee> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Attendee> attendees) {
        this.attendees = attendees;
    }

    public float getPercent_complete() {
        return percent_complete;
    }

    public void setPercent_complete(float percent_complete) {
        this.percent_complete = percent_complete;
    }

    public int getIsort() {
        return isort;
    }

    public void setIsort(int isort) {
        this.isort = isort;
    }

    @Override
    public String toString() {
        return "TagDTO{" +
                "id=" + id +
                ", name_tag='" + name + '\'' +
                ", label='" + label + '\'' +
                ", status='" + status + '\'' +
                ", start_date=" + start_date +
                ", end_date=" + end_date +
                ", list=" + listBoard +
                ", detail=" + checkList +
                '}';
    }
}
